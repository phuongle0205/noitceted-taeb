﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAudio : MonoBehaviour
{
    [ContextMenu("Get audio clip information")]
    public void TestSound() {
        AudioSource audioSource = GetComponent<AudioSource>();
        int frequency = audioSource.clip.frequency;
        int samples = audioSource.clip.samples;
        int channel = audioSource.clip.channels;
        int outputSampleRate = AudioSettings.outputSampleRate;
        Debug.Log("frequency: " + frequency + ", samples: " + samples + ", channel: " + channel + ", outputSampleRate: " + outputSampleRate);
    }
}
